#!/bin/sh

ln -sfT "$(nix-build . --no-out-link)/bin/dwm" "${XDG_DATA_HOME:-$HOME/.local/share}/bin/dwm"
