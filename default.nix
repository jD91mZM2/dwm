{
  pkgs ? import <nixpkgs> {},
  dwm ? pkgs.dwm,
}:

dwm.overrideAttrs (attrs: {
  src = ./.;
})
